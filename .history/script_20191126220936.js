(function() {
    $(document ).ready(function() {
        getVerse();
    });
  
    $("button").click(function(){
      getVerse();
    });
  
    var getVerse = function() {
    
        $.ajax({
          url: "https://labs.bible.org/api/?passage=random&type=json&callback=myCallback", 
          crossDomain: true,
          dataType: 'jsonp',
          success: function(res){
             $("#newQuote")
               .html(
                //    '<strong>'+
                     res[0].bookname+
                     ' ' + res[0].chapter +
                     ':' + res[0].verse +
                    //  '</strong>' +
                     res[0].text
                     );
            
          }
        });
    }
  })();
  